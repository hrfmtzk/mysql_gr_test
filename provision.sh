#!/bin/sh

ANSIBLE_PLAYBOOK=$1
ANSIBLE_INVENTORY=$2
ANSIBLE_HOST=$3

if [ ! -f /vagrant/$ANSIBLE_PLAYBOOK ]; then
    echo "Cannot find Ansible playbook"
    exit 1
fi

if ! [ `which ansible` ]; then
    sudo yum install -y epel-release
    sudo yum install -y ansible
fi

echo "Running Ansible"
bash -c "ansible-playbook /vagrant/${ANSIBLE_PLAYBOOK} -i /vagrant/${ANSIBLE_INVENTORY} -l ${ANSIBLE_HOST}"
